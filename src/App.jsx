import React, { Component } from "react";
import formattedFlightDates from "./flightDates";
import MonthlyCalendar from "./MonthlyCalendar";
import months from "./months";

class App extends Component {
  state = {
    flightDates: [],
    focusedDate: ""
  };

  componentDidMount() {
    this.getFlightDates();
  }

  getFlightDates() {
    // Here, we would normally query the database to get all flight dates back
    // To keep this task React-centric, the flightDates.js file is imported instead
    // and stored in state
    /*** example query ***/
    // assuming that axios has been imported:
    // axios
    //   .get("/flights")
    //   .then(({ data }) => {
    //     this.setState({
    //       flightDates: data
    //     });
    //   })
    //   .catch(err => console.err(`Error getting flight dates: ${err}`));
    this.setState({
      flightDates: formattedFlightDates
    });
  }

  setFocusedDate = (focusedDate) => {
    // e.preventDefault();
    this.setState({
      focusedDate
    });
  };

  render() {
    return (
      <div
        className="App"
        style={{
          textAlign: "center",
          alignItems: "center",
          justifyContent: "center",
          alignContent: "center"
        }}
      >
        <header
          className="App-header"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#22252B",
            height: "80px",
            padding: "20px",
            color: "#FFF"
          }}
        >
          <img
            src="blackbird-logo.jpeg"
            height="40px"
            width="40px"
            alt="blackbird"
          />
          &nbsp;
          <h1
            className="App-title"
            style={{
              fontSize: "1.7em"
            }}
          >
            Blackbird
          </h1>
        </header>
        <span
          style={{
            float: "left",
            color: "rgb(145, 55, 55)",
            fontSize: "14px",
            margin: "10px 3px 0px 30px"
          }}
        >
          &diams;
        </span>
        <span
          style={{
            float: "left",
            wrap: "true",
            margin: 10,
            marginLeft: 0,
            fontColor: "#888",
            fontSize: 14
          }}
        >
          flights scheduled
        </span>
        <br />
        {months.map((month, i) => {
          return (
            <div key={i}>
              <MonthlyCalendar
                key={i}
                month={month}
                flights={this.state.flightDates}
                focusedDate={this.state.focusedDate}
                setFocusedDate={this.setFocusedDate}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

export default App;
