import React from "react";
import PropTypes from "prop-types";
import moment from "moment";

// This way of checking for flight date matches works under the assumption that
// it is the current year, which is not necessarily what we should assume
const DateSquare = ({ month, date, flights, setFocusedDate, focusedDate }) => {
  // map over flights array to isolate each date number
  const flightDateNumbers = flights.map(date =>
    Number(date.slice(month.length + 1, -6))
  );

  // iterates over flightDateNumbers to check for date match
  const hasFlight = false || flightDateNumbers.indexOf(date) !== -1;

  // format date as "MMMM DD, YYYY"
  const dateString = `${month} ${date}, ${moment().format("YYYY")}`;

  const focusedStyle = {
    border: dateString === focusedDate ? "solid 1px rgb(145, 55, 55)" : "none"
  };

  return (
    <li onClick={() => setFocusedDate(dateString)}>
      {hasFlight ? (
        <div style={focusedStyle}>
          <span style={{ color: "rgb(145, 55, 55)" }}>&diams;</span>
          {date}
        </div>
      ) : (
        <div style={focusedStyle}>{date}</div>
      )}
    </li>
  );
};

export default DateSquare;

DateSquare.propTypes = {
  month: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  flights: PropTypes.arrayOf(PropTypes.string).isRequired,
  setFocusedDate: PropTypes.func.isRequired
};
