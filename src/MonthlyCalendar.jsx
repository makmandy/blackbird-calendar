import React from "react";
import PropTypes from "prop-types";
import "./MonthlyCalendar.css";
import moment from "moment";
import DateSquare from "./DateSquare";

const MonthlyCalendar = ({ month, flights, setFocusedDate, focusedDate }) => {
  // Sorted array of the dates of a given month
  const getMonthDates = monthString => {
    const monthDates = [];
    const numberOfDays = moment(monthString, "MMMM").daysInMonth();
    for (let i = 1; i <= numberOfDays; i++) {
      monthDates.push(i);
    }
    return monthDates;
  };

  const datesOfMonth = getMonthDates(month);

  // An integer that represents a day of the week,
  // where 0 represents Sunday and 6 represents Saturday
  const getWeekdayOfThe1st = date => {
    const dayCode = moment(date, "YYYY-MMMM-DD").format("d");
    return dayCode;
  };

  // Array with the length of the days to shift in order to align the dates
  // with the correct day of week
  // (Having just the number is insufficient for rendering repeated elements in React, I think)
  const daysToShift = new Array(
    Number(getWeekdayOfThe1st(`${moment().format("YYYY")}-${month}-01`))
  ).fill("0");

  // Filtered array of flight dates specific to the component's month
  // This will be passed rather than the flights array to DateSquare for shorter iteration
  const filteredFlights = flights
    ? flights.filter(date => date.includes(month))
    : flights;

  return (
    <div
      className="calendar"
      style={{
        width: 400,
        display: "fixed",
        margin: 10,
        padding: 10,
        height: 330
      }}
    >
      <div className="month">
        <ul>
          <li>
            {month}
            <br />
            {moment().format("YYYY")}
          </li>
        </ul>
      </div>
      <ul className="weekdays">
        <li>Su</li>
        <li>M</li>
        <li>T</li>
        <li>W</li>
        <li>Th</li>
        <li>F</li>
        <li>Sa</li>
      </ul>

      <ul className="days">
        {daysToShift.map(i => (
          <li> </li>
        ))}
        {datesOfMonth.map(date => (
          <DateSquare
            date={date}
            month={month}
            flights={filteredFlights}
            setFocusedDate={setFocusedDate}
            focusedDate={focusedDate}
          />
        ))}
      </ul>
    </div>
  );
};

export default MonthlyCalendar;

MonthlyCalendar.propTypes = {
  month: PropTypes.string.isRequired,
  setFocusedDate: PropTypes.func.isRequired,
  flights: PropTypes.arrayOf(PropTypes.string).isRequired
};
