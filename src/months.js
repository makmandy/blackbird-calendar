const moment = require("moment");

const months = moment.months();

export default months;
